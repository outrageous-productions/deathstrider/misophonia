# Misophonia

A sound addon for [Deathstrider](https://gitlab.com/accensi/deathstrider).

---

For anyone who doesn't like heartbeat/gulping sounds and similar.

Heartbeat replaced with a small blip/click sound.
Drinking replaced with a cork/slosh sound.
Bloodbag attaching replaced with a more injector-y sound.
Bloodbag removal replaced with a tape-like sound.
Breathing for the Spicy Air addon replaced with something somewhat more artificial-sounding.

---

See [credits.txt](./credits.txt) for attributions.
